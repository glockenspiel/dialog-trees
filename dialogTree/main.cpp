#include <iostream>
#include <string>
#include "option.h"
#include <vector>
#include "loadDTree.h"


using namespace std;

int main(){
	//create options
	/*
	Option* root = new Option("how are you?");
	Option* a = new Option("thats good to hear");
	Option* b = new Option("well then, that doesnt sound great");
	Option* name = new Option("what is your name?");
	Option* resName = new Option("That is a nice name!");
	Option* whereis = new Option("its in west clare");
	Option* help = new Option("no ask someone else");
	Option* info = new Option("not much to say only im a pimp");
	Option* loopBase = new Option("what else do you want to talk about?");

	//link options
	root->addOption("ok", a);
	root->addOption("good", a);
	root->addOption("bad", b);

	a->addAutoOption(name);
	b->addAutoOption(name);

	name->addOption("mikey the pikey", resName);
	name->addOption("joan of ark", resName);

	resName->addAutoOption(loopBase);
	loopBase->addOption("where is mullagh?", whereis);
	loopBase->addOption("can you help me with this?", help);
	loopBase->addOption("tell me about you?", info);
	loopBase->addOption("Goodbye", nullptr);

	whereis->addAutoOption(loopBase);
	help->addAutoOption(loopBase);
	info->addAutoOption(loopBase);
	*/
	vector<Option*> trees = LoadDTree::load("file.txt");

	//start loop
	bool flag = true;
	static Option* o = trees.at(0);
	static unsigned int index = -1;
	while (o!=nullptr){
		cout << o->getMsg() << endl;
		if (o->hasOptions()){ //check if auto ptr
			cout << o->getOptions() << endl;
			
			//validate input
			flag = true;
			while (flag){
				cout << "Enter option: ";
				if (cin >> index && index <= o->countOpt())
					flag = false;
				else{
					cin.clear();
					cin.ignore(numeric_limits<streamsize>::max(), '\n');
					cout << "Invalid input must be in range of " << 1 << "-" << o->countOpt()  << endl;
				}
			}

			system("cls");
			o = o->choose(index);
		}
		else {
			system("pause");
			o = o->getAutoOption();
		}
	}

	return 0;
}
