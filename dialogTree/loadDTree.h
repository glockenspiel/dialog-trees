#ifndef LOAD_DTREE_H
#define LOAD_DTREE_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "option.h"
#include <sstream>
#include <map>

using std::string;

class LoadDTree{
public:
	static vector<Option*> load(string filename);
};
#endif