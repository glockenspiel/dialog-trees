#include "option.h"

Option::Option(string text){
	msg = text;
}

string Option::getMsg(){
	return msg;
}

string Option::getOptions(){
	string s;
	for (unsigned int i = 0; i < choicesText.size(); i++){
		s += to_string(i + 1) + "." + choicesText.at(i) + "\n";
	}
	return s;
}

void Option::addOption(string reply, Option* opt){
	choices.push_back(opt);
	choicesText.push_back(reply);
}

Option* Option::choose(unsigned int index){
	if (index - 1 < choices.size())
		return choices.at(index - 1);
	return nullptr;
}

void Option::addAutoOption(Option* opt){
	autoOption = opt;
}

Option* Option::getAutoOption(){
	return autoOption;
}

bool Option::hasOptions(){
	if (autoOption == nullptr)
		return true;
	return false;
}

int Option::countOpt(){
	return choices.size();
}