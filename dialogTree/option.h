#ifndef OPTION_H
#define OPTION_H

#include <string>
#include <vector>

using namespace std;

class Option{
public:
	Option(string text);
	string getMsg();
	void Option::addOption(string reply, Option* opt);
	Option* choose(unsigned int index);
	void addAutoOption(Option* opt);
	Option* getAutoOption();
	string getOptions();
	bool hasOptions();
	int countOpt();
	string getDisplay();
private:
	Option* autoOption = nullptr;
	string msg;
	vector<Option*> choices;
	vector<string> choicesText;
};
#endif