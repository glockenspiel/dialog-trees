#include "loadDTree.h"

/*
file structure

(tree)~
id|msg~
id|msg~
id|msg~
...
...

(linkers)~
id|autoOptionID|text|choiceID~
id|autoOptionID|text|choiceID~
id|autoOptionID|text|choiceID~
...
...
*/


vector<Option*> LoadDTree::load(string filename){
	map<int, Option*> options; //option ptrs, used for linking later
	vector<Option*> trees; //root options to be returned
	vector<string> items; //input file strings
	vector<int> rootIDs;

	string line, token;
	int id, autoID, choiceID;
	bool once = false, readOptions = true;

	ifstream infile;
	infile.open(filename);
	while (getline(infile, line, '~')){ //each line
		if (line == "(tree)" || line == "\n(tree)"){
			once = true;
			readOptions = true;
		}
		else if (line == "\n(linkers)" || line == "(linkers)"){
			readOptions = false;
		}
		else{
			std::istringstream ss(line);

			//read each item into vector
			items.clear();
			while (getline(ss, token, '|'))
				items.push_back(token);
			if (items.size() >= 2){ //valid line
				id = stoi(items.at(0));
				if (readOptions){ //reading options
					options[id] = new Option(items[1]);

					if (once){ //store root id
						rootIDs.push_back(id);
						once = false;
					}
				}
				else{ //reading linkers
					if (items[1].length() > 0){ //has an autoOption
						autoID = stoi(items[1]);
						options[id]->addAutoOption(options[autoID]);
					}
					else{ //regular option
						if (items[3] == "null"){ //exit option
							options[id]->addOption(items[2], nullptr);
						}
						else{
							choiceID = stoi(items[3]);
							options[id]->addOption(items[2], options[choiceID]);
						}
					}
				}//end of item
			}
		}//end of line
	} //end of file
	//populate trees with root options
	for (int i = 0; i < rootIDs.size(); i++)
		trees.push_back(options[rootIDs.at(i)]);
	return trees;
}